// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

function lastCar( inventory ){
    const carData = inventory.filter((carInfo) => {
        return carInfo;
    });
    return (carData.length>0 ? carData[carData.length-1] : null);
}

module.exports = { lastCar };