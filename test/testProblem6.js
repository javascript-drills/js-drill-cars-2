const { inventory } = require("../dataSet");
const { getSpecificCars } = require("../problem6");

const bmwAudiList = getSpecificCars(inventory);

if(bmwAudiList){
    console.log(JSON.stringify(bmwAudiList));
}
else{
    console.log("No BMW or Audi cars available.")
}