const { inventory } = require("../dataSet");
const { getOldCars } = require("../problem5");

const oldCarList = getOldCars(inventory);

if(oldCarList == null){
    console.log("No data available");
}
else{
    console.log(`Total number of cars before 2000 are: ${oldCarList}`)
}