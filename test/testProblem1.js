const { inventory } = require("../dataSet");
const { carInfo } = require("../problem1");

const carById = carInfo(inventory, 33);

if( carById === null ){
    console.log("No data available.")
}
else{
    console.log(`Car 33 is a ${carById.car_year} ${carById.car_make} ${carById.car_model}`);
}