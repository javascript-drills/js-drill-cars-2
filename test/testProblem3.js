const { inventory } = require("../dataSet");
const { sortCars } = require("../problem3");

const carList = sortCars(inventory);

if( carList ){
    console.log(carList);
}
else{
    console.log("No data available");
}