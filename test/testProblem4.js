const { inventory } = require("../dataSet");
const { getYears } = require("../problem4");

const carYearList = getYears(inventory);

if(carYearList){
    console.log(carYearList);
}
else{
    console.log("No data available");
}