const { inventory } = require("../dataSet");
const { lastCar } = require("../problem2");

const lastCarInfo = lastCar(inventory);

if( lastCarInfo == null){
    console.log("No car data");
}
else{
    console.log(`Last car is a ${lastCarInfo.car_make} ${lastCarInfo.car_model}`);
}